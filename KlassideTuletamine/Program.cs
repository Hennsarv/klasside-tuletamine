﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            //Loom l = new Loom("krokodill");
            //Console.WriteLine(l);
            //l.TeeHäält();

            //Loom k = new Loom(jalgadeArv: 3);
            //Console.WriteLine(k);

            //Loom o = new Loom { Liik = "kaheksajalg", JalgadeArv = 8 };
            //Console.WriteLine(o);

            Kass kiisu = new Kass("siiam", "Miisu");
            kiisu.LõikaJalg(-1);


            Console.WriteLine("alguses teeb kiisusellist häält");
            kiisu.TeeHäält();
            kiisu.SikutSabast();
            kiisu.TeeHäält();
            kiisu.Silita();
            kiisu.TeeHäält();

            Koer kutsa = new Koer("granz", "Pontu");
            kutsa.TeeHäält();

            Console.WriteLine("\nLoomaaed\n");
            foreach (var x in Loom.Loomaaed)
            {
                Console.WriteLine(x);
                x.TeeHäält();
            }

            // muutuja võib olla ka interface tüüpi
            // selle väärtuseks võib olla suva, kes implementeerib
            // seda interfacet
            ISöödav i = new Sepik();
            i.Söömine();
            i = kutsa;
            i.Söömine();

            foreach (var x in Loom.Loomaaed)
                if (x is ISöödav) ((ISöödav)x).Söömine();


            List<Koer> koerad = new List<Koer>
            {
                new Koer("grantz", "Polla"),
                new Koer("grantz", "Pontu"),
                new Koer("kolli", "Lessi"),
                new Koer("grantz", "Rex"),
                new Koer("grantz", "Lible"),
                new Koer("grantz", "Adalbert")
            };

            koerad.Sort();
            foreach (var x in koerad) Console.WriteLine(x.Nimi);



        }
    }

    // teeme natuke lõbusamaks asja ja siis läheme tõsisemaks tagasi
    // mõne jaoks läheb nüüd lugu keeruliseks
    // mõne jaoks vastupidi lihtsamaks

    // kujutame ette rakendus, mis tegeleb loomadega 
    // (miski arvepidamine, loomaaed, vms...)
    // Kõigil loomadel on midgi ühetaolist
    // samas on koduloomadel ja metsloomadel ka miskid erinevused
    // lemmikloomad ja muud koduloomad erinevad pisut
    // (koduloomi süüakse, lemmikloomi ei sööda)
    // ka lemmikloomad Kass ja Koer toimivad pisut erinevalt

    // interface on ÜLIABSTRAKTNE klass
    // siin ei ole konkreetseid asju
    // on vaid meetodite signatuurid
    // meetodid ise tuleb tuletatud klassides implementeerida
    interface ISöödav
    {
        // interfaces on vaid meetodite signatuurid
        void Söömine();
    }


    class Sepik : ISöödav
    {
        public void Söömine()
        {
            Console.WriteLine("Keegi nosib sepikut") ;
        }
    }


    // lisaks baasklassile võib olla kuitahes palju interfacesid

    class Koer : Koduloom, ISöödav, IComparable
    {
        public string Tõug;
        
        public Koer (string tõug, string nimi) : base ("koer")
        {
            Tõug = tõug;
            Nimi = nimi;
        }

        public int CompareTo(object obj)
        {
            if (obj is Koer)
            { return 
                    Tõug.CompareTo(((Koer)obj).Tõug) == 0 ? 
                    Nimi.CompareTo(((Koer)obj).Nimi) :
                    Tõug.CompareTo(((Koer)obj).Tõug)

                    ; }
            else return 1;
        }

        public void Söömine()
        {
            Console.WriteLine($"koer {Nimi} pistetakse nahka") ;
        }

        public override void TeeHäält()
        {
            Console.WriteLine($"{Nimi} haugub");
            
        }

        public override string ToString()
        {
            return $"Koer {Nimi} (tõug: {Tõug})";
        }


    }


    // järgmine tuöletatud klass
    class Kass : Koduloom
    {
        public string Tõug;
        public bool Tuju = true;

        // tuletatud klassil on oma konstrukoprid
        // konstruktorid baasklassist kaasa ei tule
        // neid aga saab välja kutsuda nagu base("kass")
        public Kass(string tõug, string nimi)  : base("kass")
        {
            Liik = "kass";
            Nimi = nimi;
            Tõug = tõug;
        }

        // kassil võib olla täiesti oma toimimisloogika
        public void Silita() => Tuju = true;
        public void SikutSabast() => Tuju = false;

        public override void TeeHäält()
        {
            Console.WriteLine($"Kass {Nimi} {(Tuju ? "lööb nurru" : "kräunub")}") ;
        }


        public override string ToString()
        {
            return $"Kiisu {Nimi} kes on tõust {Tõug} (jalgu tal on {JalgadeArv})";
        }

    }
   

    // tuletatud klass
    // tuletatud klassil ON kõik see, mis baasklassil
    // lisaks võib olla nii välju kui muid asju
    // abstraktsed meetodid baasklassist TULEB implementeerida (overridida)
    // virtuaalsed meetodid VÕIB üle defineerida
    class Koduloom : Loom
    {
        public string Nimi;

        public Koduloom(string liik = "tundmatu koduloom") : base(liik, 4)
        { }

        public override void TeeHäält()
        {
            Console.WriteLine($"miski koduloom {Nimi} lõugab") ;
        }
    }


    // abstraktne baasklass
    // alguses ei olnud ta abstraktne, 
    // aga me tegime Hääletegemise meetodi
    // abstraktseks

    // muutuja saab olla abstraktset tüüpi
    // new abstraktsele tüübile teha ei saa

    abstract class Loom
    {
        public static List<Loom> Loomaaed = new List<Loom>();
        public string Liik;
        public int JalgadeArv;

        public Loom(string liik = "tundmatu liik", int jalgadeArv = 4)
        {
            Liik = liik;
            Loomaaed.Add(this);
            JalgadeArv = jalgadeArv;
        }

        public void LõikaJalg(int mitu = 1) { if (JalgadeArv > mitu) JalgadeArv -= mitu; }

        //        public Loom() : this("tundmatu liik") { }


        // abstraktse meetodi signatuur
        // ilma bodita meetod
        public abstract void TeeHäält();
        //{
        //    Console.WriteLine($"loom liigist {Liik} teeb koledat häält");
        //}



        public override string ToString()
        {
            return $"loom liigist {Liik} tal on {JalgadeArv} jalga";
        }
    }

}
